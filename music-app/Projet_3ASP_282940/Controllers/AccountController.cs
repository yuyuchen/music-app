﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Projet_3ASP_282940.Models;
using Projet_3ASP_282940.Data;
using Microsoft.EntityFrameworkCore;




namespace Projet_3ASP_282940.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Index","Home");
        }

        //GET: Account/Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        //GET: Account/Register
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {               
                IdentityUser user = new IdentityUser()
                {
                    UserName = model.Name,
                    Email = model.Email
                };                
            
                var result = await _userManager.CreateAsync(user, model.Password);             
              
                if (result.Succeeded)
                {
                    Account UserAccount = new Account
                    {
                        Name = user.UserName,                     
                        PlayLists = new List<PlayList>()
                        {
                            new PlayList() 
                            {
                                PlayListName="Download",
                                Musics = new List<Music>()                            
                            }
                        }                       
                    };
                    _context.Accounts.Add(UserAccount);                   
                    await _context.SaveChangesAsync();
                 
                    string code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(code));
                    await _userManager.ConfirmEmailAsync(user, code);
        
                    return RedirectToAction("RegisterMessage", "Account");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }             
            }
            return View(model);
        }


        public ActionResult RegisterMessage()
        {
            return View();
        }
        

        [HttpPost]
        public async Task<IActionResult> Login (LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);


                if (result.Succeeded)
                {
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "PlayList");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                }

            }
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            Response.Cookies.Delete("PlayListIDToPlay");
            Response.Cookies.Delete("MusicToPlay");
            return RedirectToAction("Index", "Home");
        }

    }
}