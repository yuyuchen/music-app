﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Projet_3ASP_282940.Models;
using Projet_3ASP_282940.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Projet_3ASP_282940.Controllers
{
    public class PlayListController : Controller
    {

        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ApplicationDbContext _context;

        public PlayListController(SignInManager<IdentityUser> signInManager, ApplicationDbContext context)
        {
            _signInManager = signInManager;
            _context = context;
        }

        //GET : Display All Playlist
        [HttpGet]
        public ActionResult Index()
        {
            if (_signInManager.IsSignedIn(User))
            {
                var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);
                var GetPlayLists = _context.PlayLists.Where(p => p.UserAccount == GetAccount);
                Response.Cookies.Delete("PlayListIDToPlay");
                Response.Cookies.Delete("MusicToPlay");

                ViewData["Theme"] = Request.Cookies["Theme"];
                return View(GetPlayLists.ToList());

            }
            return RedirectToAction("Error");
        }
      
        //GET
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(PlayList model)
        {
            if (ModelState.IsValid & _signInManager.IsSignedIn(User))
            {
                try
                {
                    var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);

                    PlayList playlist = new PlayList()
                    {
                        PlayListName = model.PlayListName,
                        UserAccount = GetAccount
                    };

                    _context.PlayLists.Add(playlist);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "PlayList");
                }
                catch
                {
                    return RedirectToAction("Error");
                }         
            }
            else
            {
                return View();
            }
        }

        //GET
        [HttpGet]
        public ActionResult Rename(int? id)
        {
            var playList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);

            if (id == null || 
                playList == null || 
                !_signInManager.IsSignedIn(User) ||
                playList.UserAccount != GetAccount)
            {
                return RedirectToAction("Error");
            }
                return View();        
        }

        [HttpPost]
        public async Task<ActionResult> Rename(PlayList Model, int? id)
        {
            var playList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);

            if (id == null ||
                playList == null ||
                !_signInManager.IsSignedIn(User) ||
                playList.UserAccount != GetAccount)
            {
                return RedirectToAction("Error");
            }

            if (ModelState.IsValid & _signInManager.IsSignedIn(User))
            {             
                try
                {
                    var PlayListToRename = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
                    PlayListToRename.PlayListName = Model.PlayListName;
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "PlayList");
                }
                catch
                {
                    return RedirectToAction("Error");
                }
                
      
                
            }
            return View(); 
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var playList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);
            var GetMusics = _context.Musics.Where(m => m.MusicPlayList == playList);

            if (id == null ||
                playList == null ||
                !_signInManager.IsSignedIn(User) ||
                playList.UserAccount != GetAccount)
            {
                return RedirectToAction("Error");
            }

            try
            {
                foreach (Music music in GetMusics)
                {
                    _context.Musics.Remove(music);
                }
                await _context.SaveChangesAsync();
                _context.PlayLists.Remove(playList);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "PlayList");
            }
            catch
            {
                return RedirectToAction("Error");
            }
        }

        public ActionResult PlayListToPlay(int? id)
        {
            var GetPlayList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            CookieOptions option = new CookieOptions();
            option.Expires = DateTime.Now.AddMinutes(10);
            Response.Cookies.Append("PlayListIDToPlay", GetPlayList.PlayListID.ToString(), option);
            Response.Cookies.Delete("MusicToPlay");

            return RedirectToAction("Index", "Player");
        }

        public ActionResult Error ()
        {
            return View();
        }

    }
}
 