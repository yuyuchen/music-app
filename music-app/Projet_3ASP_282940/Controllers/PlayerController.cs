﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projet_3ASP_282940.Data;
using Projet_3ASP_282940.Models;
using Microsoft.AspNetCore.Http;

namespace Projet_3ASP_282940.Controllers
{
    public class PlayerController :Controller
    {

        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ApplicationDbContext _context;

        public PlayerController(SignInManager<IdentityUser> signInManager, ApplicationDbContext context)
        {
            _signInManager = signInManager;
            _context = context;
        }
  
        public ActionResult Index()
        {
            if (_signInManager.IsSignedIn(User))
            {
                try
                {

                    var id = Request.Cookies["PlayListIDToPlay"];
                    ViewData["PlayListID"] = id;       
                    var playlist = _context.PlayLists.FirstOrDefault(p => p.PlayListID == Int32.Parse(id));
                    var musicsList = _context.Musics.Where(m => m.MusicPlayList == playlist).ToList();

                    var musicToPlay = Request.Cookies["MusicToPlay"];
                    if (musicToPlay != null)
                        ViewData["MusicToPlay"] = musicToPlay;

                    return View(musicsList);
                    
                }
                catch
                {
                    return RedirectToAction("Error");
                }
                
            }
            else
            {
                return RedirectToAction("Error");
            }
            
        }

        public ActionResult Next(int? id)
        {
            if (id == null) return RedirectToAction("Error");
            else return RedirectToAction("Index", "Player", new { id });

        }

        public ActionResult Error()
        {
            return View();
        }
    }
}
